const axios = require('axios');
const fs = require('fs');

if (!process.argv[2]) {
    console.log('Error: no token provided.')

    return process.exit();
}

const token = process.argv[2];

const getOffsetedConversationsMembers = (offset) => axios({
    method: 'get',
    url: `https://api.vk.com/method/messages.getConversations?access_token=${token}&v=5.80&count=200&offset=${offset}`,
})
    .then((res) => res.data.response.items.map((item) => item.conversation.peer.id))
    .catch((err) => console.log('Error on sub-requests: ', err));

axios({
    method: 'get',
    url: `https://api.vk.com/method/messages.getConversations?access_token=${token}&v=5.80&count=200`,
})
    .then(async (res) => {
        console.log('Dialogs found: ', res.data.response.count);

        const conversationsCount = res.data.response.count;
        let result = [];

        for (let i = 0; i < conversationsCount; i += 200) {
            const members = await getOffsetedConversationsMembers(i);

            if (Array.isArray(members)) {
                result = result.concat(members);
            }

            await setTimeout(() => '', 100);
        }

        fs.writeFile(`${__dirname}/result.txt`, result.join('\n'), 'utf8', (err) => {
            if (err) {
                return console.log('Error on fs operation: ', err);
            }

            console.log('Parsing is finished! Check result.txt')
        });
    })
    .catch((err) => console.log('Error on initial request, check your token: ', err));

